include_recipe 'nginx'

# Disable default vhost to avoid conflict with port 80
nginx_site 'default' do
  enable false
end

php_fpm_pool 'default' do
  user 'www-data'
  group 'www-data'

  action [ :install ]
end

template '/etc/nginx/sites-available/myapp' do
   source 'nginx_vhost.conf.erb'
   owner 'root'
   group 'root'
   mode  '0644'
end

nginx_site 'myapp'

service 'nginx' do
  action :restart
end