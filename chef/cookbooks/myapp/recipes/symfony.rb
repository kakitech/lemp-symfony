execute 'install_symfony_clone_repository' do
  command 'cd /var/www && git init . && git remote add ' + node['myapp']['app']['repository_remote_name'] + ' ' + node['myapp']['app']['repository'] + ' && git pull ' + node['myapp']['app']['repository_remote_name'] + ' ' + node['myapp']['app']['repository_branch']
end

execute 'install_symfony_run_composer_install' do
  command 'cd /var/www && /usr/local/bin/composer install'
end

execute 'install_symfony_clear_cache' do
	command 'cd /var/www && rm -rf var/cache/* var/logs/* var/sessions/*'
end