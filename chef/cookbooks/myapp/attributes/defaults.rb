default['myapp']['database']['host']             = 'localhost'
default['myapp']['database']['version']          = '5.5'
default['myapp']['database']['socket']           = '/var/run/mysqld/mysqld.sock'
default['myapp']['database']['username']         = 'root'
default['myapp']['database']['password']         = 'JkvmqJRbzReX44K'
default['myapp']['database']['dbname']           = 'myapp'

default['myapp']['database']['api']['username']  = 'myapp_user'
default['myapp']['database']['api']['password']  = '2RVNtHRPJTsmG2P'

default['php']['directives']                     = {
	'date.timezone' => 'Asia/Singapore',
	'short_open_tag' => 'Off',
  	'post_max_size' => '1G',
  	'upload_max_filesize' => '1G'
}

# Web Root
default['myapp']['app']['dir']                   = '/var/www'

# Application folder
default['myapp']['app']['folder']                = '/var/www'

# Application Repository
default['myapp']['app']['repository']            = 'https://github.com/symfony/symfony-standard.git'

# Application Repository Branch
default['myapp']['app']['repository_branch']     = '3.0'

# Application Repository Remote Name ( donot use origin )
default['myapp']['app']['repository_remote_name']= 'kakisoft'