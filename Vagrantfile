# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure(2) do |config|
  
  # VM hostname
    config.vm.hostname = 'myapp-vm'

  # Vagrant Box
    config.vm.box = "ubuntu/trusty64"
    config.vm.box_url = "https://atlas.hashicorp.com/ubuntu/boxes/trusty64"


  # Network settings
    config.vm.network "private_network", ip: "10.0.0.100", mac: "000C2959C497"
  
  # Port Forward http and mysql port  
    config.vm.network "forwarded_port", guest: 8080, host: 8080
    config.vm.network "forwarded_port", guest: 3306,  host: 3306
  
  # Share folder
  #  config.nfs.map_uid = Process.uid
  #  config.nfs.map_gid = Process.gid  
  #  config.vm.synced_folder "www", "/var/www", type: "nfs"    

 config.vm.synced_folder "www", "/var/www/",
    id: "vagrant-root",
    owner: "vagrant",
    group: "www-data",
    mount_options: ["dmode=775,fmode=664"]     

  # Provider-specific configuration so you can fine-tune various
    config.vm.provider "virtualbox" do |vb|
      # Customize the amount of memory on the VM:
      vb.memory = "2048"
      vb.customize ["modifyvm", :id, "--memory", "2048"]
    end

  # Get latest chef version
  # NOTE: You will need to install the vagrant-omnibus plugin:
  #
  #   $ vagrant plugin install vagrant-omnibus
  #
    if Vagrant.has_plugin?("vagrant-omnibus")
      config.omnibus.chef_version = 'latest'
    end


  # The path to the Berksfile to use with Vagrant Berkshelf
    config.berkshelf.berksfile_path = "./Berksfile"

  # Enable Berkshelf
  # Download and install ChefDK from here https://downloads.chef.io/chef-dk/
  # Before you run $ berks install
    config.berkshelf.enabled = true


  # Provisioning
  # Coookbook folder structure
  # $ tree
  # .
  # |-- Vagrantfile
  # |-- chef
  # |   |-- cookbooks
  # |       |-- myapp
  # |           |-- recipes
  # |               |-- default.rb
    config.vm.provision "chef_solo" do |chef|
      chef.cookbooks_path = ["cookbooks", "chef/cookbooks"]    
      chef.add_recipe "myapp"
    end


end
